import re
import sys

if len(sys.argv) != 2:
    sys.exit(1)

filename = sys.argv[1]
pattern = r"(\d+);([-]?\d+.\d+)\.*"

with open(filename, "r") as f:
    content = f.read()
    for s in content.split('\n'):
        if ';' not in s:
            continue
        m = re.search(pattern, s)
        print m.group(1), m.group(2)
