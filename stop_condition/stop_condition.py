import time

from pyage.core.stop_condition import StopCondition


class TimeStopCondition(StopCondition):
    def __init__(self, computation_time):
        super(TimeStopCondition, self).__init__()
        self.start = time.time()
        self.computation_time = computation_time

    def should_stop(self, workplace):
        return time.time() - self.start >= self.computation_time