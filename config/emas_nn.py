import logging
import os

from pyage.core import address
from pyage.core.agent.agent import unnamed_agents
from pyage.core.agent.aggregate import AggregateAgent
from core.emas import EmasService
from pyage.core.locator import TorusLocator
from pyage.core.migration import ParentMigration

from evolution.crossover import NNCrossover
from evolution.evaluation import NeuralNetworkEvaluation
from evolution.initializer import float_nn_initializer
from evolution.mutation import NNMutation
from stats.statistics import EmasNNTimeStatistics
from stop_condition.stop_condition import TimeStopCondition

logger = logging.getLogger(__name__)

agents_count = int(os.environ['AGENTS'])
logger.debug("EMAS, %s agents", agents_count)
agents = unnamed_agents(agents_count, AggregateAgent)

stop_condition = lambda: TimeStopCondition(20000)

in_size = 13
out_size = 3

aggregated_agents = lambda: float_nn_initializer(energy=100, size=60, in_size=in_size, out_size=out_size)

emas = EmasService

minimal_energy = lambda: 0
reproduction_minimum = lambda: 120
migration_minimum = lambda: 130
newborn_energy = lambda: 100
transferred_energy = lambda: 5

evaluation = lambda: NeuralNetworkEvaluation(data_set=os.path.join('utils', 'wine.data'), in_size=in_size, out_size=out_size)
crossover = NNCrossover
mutation = lambda: NNMutation(probability=1)

address_provider = address.SequenceAddressProvider

migration = ParentMigration
locator = lambda: TorusLocator(10, 10)

stats = lambda: EmasNNTimeStatistics(20, 'emas_nn.txt')
