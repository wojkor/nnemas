import random

from pyage.solutions.evolution.mutation import AbstractMutation

from evolution.genotype import NNGenotype

from neural_network.network import NeuralNetwork

class NNMutation(AbstractMutation):
    def __init__(self, probability=0.1, hidden_neurons_upperbound=20):
        super(NNMutation, self).__init__(NNGenotype, probability)
        self.hidden_neurons_upperbound = hidden_neurons_upperbound
        self.probability = probability

    def mutate(self, genotype):
        genotype.genes[random.randint(0, NeuralNetwork.get_weights_count(genotype.genes) - 1)+4] = random.uniform(-3, 3)
