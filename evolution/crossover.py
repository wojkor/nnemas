import random

from pyage.solutions.evolution.crossover import AbstractCrossover

from evolution.genotype import NNGenotype

from neural_network.network import NeuralNetwork


class NNCrossover(AbstractCrossover):
    def __init__(self, size=100):
        super(NNCrossover, self).__init__(NNGenotype, size)

    def cross(self, p1, p2):
        if random.random() > 0.5:
            p = p1
            p1 = p2
            p2 = p
        new_genes = []
        split = random.randint(1, NeuralNetwork.get_weights_count(p1.genes))
        new_genes.extend(p1.genes[:split])
        new_genes.extend(p2.genes[split:])
        return NNGenotype(new_genes)
