import random
import numpy

from core.emas import EmasAgent

from evolution.genotype import NNGenotype

def float_nn_initializer(energy=10, size=100, in_size=3, out_size=1, hidden_layer_size=15):
        agents = {}
        for i in range(size):
                d = [in_size, out_size, hidden_layer_size, (in_size+1)*hidden_layer_size + (hidden_layer_size+1)*out_size]
                d.extend([random.uniform(-3, 3) for _ in xrange(0, d[3])])
                agent = EmasAgent(NNGenotype(d), energy)
                agents[agent.get_address()] = agent
        return agents
