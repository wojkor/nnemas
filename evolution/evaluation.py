import math

from pyage.core.operator import Operator

from evolution.genotype import NNGenotype
from neural_network.network import NeuralNetwork
from utils.data_reader import get_data_set
from utils.utils import counted


class NeuralNetworkEvaluation(Operator):
    def __init__(self, data_set, in_size, out_size):
        super(NeuralNetworkEvaluation, self).__init__(NNGenotype)
        self.data_set = get_data_set(data_set, in_size, out_size)
        self.in_size = in_size
        self.out_size = out_size 

    def process(self, population):
        for genotype in population:
            f = self._evaluate(genotype.genes)
            if math.isnan(f):
                f = float('inf')
            genotype.fitness = f

    @counted
    def _evaluate(self, genes):
        network = NeuralNetwork(in_size=self.in_size, out_size=self.out_size, dataset=self.data_set)
        return network.get_fitness(genes)

