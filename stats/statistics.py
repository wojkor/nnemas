import logging
import time
from math import sqrt

from pyage.core.inject import Inject
from pyage.core.statistics import Statistics

logger = logging.getLogger(__name__)


class StepStatistics(Statistics):
    @Inject("evaluation")
    def __init__(self, interval, output_file_name='fitness_pyage.txt'):
        self.history = []
        self.interval = interval
        self.fitness_output = open(output_file_name, 'a')

    def __del__(self):
        self.fitness_output.close()

    def append(self, best_fitness, step_count, agents_count, eval_calls, diversity):
        self.fitness_output.write(';'.join(
            map(str, [step_count - 1, abs(best_fitness), eval_calls, agents_count, diversity[0], diversity[1]])) + '\n')
        self.fitness_output.flush()

    def update(self, step_count, agents):
        try:
            best_fitness = max(a.get_fitness() for a in agents)
            # print sum(aa.get_energy() for a in agents for aa in a.get_agents())
            logger.info(best_fitness)
            self.history.append(best_fitness)
            if (step_count - 1) % self.interval == 0:
                print step_count
                agents_count = sum(len(a.get_agents()) for a in agents)
                eval_calls = self.evaluation._evaluate.called
                diversity = self.compute_diversity(agents)
                self.append(best_fitness, step_count, agents_count, eval_calls, diversity)
        except:
            logging.exception("")

    def summarize(self, agents):
        pass

    def compute_diversity(self, agents):
        agents_max = []
        sum_jong = 0.0
        jong_values = []

        for a in agents:
            agent_genotypes = [ag.get_genotype().genes for ag in a.get_agents()]
            avg_vals = [0] * len(agent_genotypes[0])
            pow_sum_vals = [0] * len(agent_genotypes[0])
            for gen in agent_genotypes:
                for i, g in enumerate(gen):
                    avg_vals[i] += g
                    pow_sum_vals[i] += g * g

            values = []
            for i in range(len(avg_vals)):
                avg_vals[i] /= len(agent_genotypes)
                values.append(sqrt(round(pow_sum_vals[i] / len(agent_genotypes) - avg_vals[i] * avg_vals[i], 7)))

            jong_value = 0.0
            for gen in agent_genotypes:
                for i, g in enumerate(gen):
                    val = (g - avg_vals[i]) ** 2
                    jong_value += val
                    sum_jong += val
            jong_values.append(jong_value)
            agents_max.append(max(values))

        return max(agents_max), sum_jong


class EmasNNTimeStatistics(Statistics):
    @Inject("evaluation")
    def __init__(self, interval, output_file_name='fitness_pyage.txt'):
        self.history = []
        self.interval = interval
        self.next_threshold = self.interval
        self.output_file_name = output_file_name
        self.output = open(self.output_file_name, 'a')
        self.start = time.time()

    def __del__(self):
        self.output.close()

    def append(self, time, best_fitness, agents_count, step_count, eval_calls, diversity, genes):
        self.output.write(';'.join(map(str, [time, abs(best_fitness), step_count - 1, agents_count, eval_calls,
                                             diversity[0], diversity[1], genes])) + '\n')
        self.output.flush()

    def update(self, step_count, agents):
        try:
            best_fitness = max(a.get_fitness() for a in agents)
            logger.info(best_fitness)

            if time.time() - self.start >= self.next_threshold:
                agents_count = sum(len(a.get_agents()) for a in agents)
                eval_calls = self.evaluation._evaluate.called
                self.history.append(best_fitness)

                self.next_threshold = int(time.time() - self.start) / 100 * 100
                print self.next_threshold, time.time() - self.start

                diversity = self.compute_diversity(agents)
                best_genotype = max(agents, key=lambda a: a.get_fitness()).get_best_genotype()

                self.append(self.next_threshold, best_fitness, agents_count, step_count, eval_calls, diversity,
                            best_genotype.genes[2:])
                self.next_threshold += self.interval
        except:
            logging.exception("")

    def summarize(self, agents):
        pass

    def compute_diversity(self, agents):
        agents_max = []
        sum_jong = 0.0
        jong_values = []

        for a in agents:
            agent_genotypes = [ag.get_genotype().genes for ag in a.get_agents()]
            max_len = max(len(gen) for gen in agent_genotypes)
            avg_vals = [0] * max_len
            pow_sum_vals = [0] * max_len
            for gen in agent_genotypes:
                for i, g in enumerate(gen):
                    avg_vals[i] += g
                    pow_sum_vals[i] += g * g

            values = []
            for i in range(len(avg_vals)):
                avg_vals[i] /= len(agent_genotypes)
                values.append(sqrt(round(pow_sum_vals[i] / len(agent_genotypes) - avg_vals[i] * avg_vals[i], 7)))

            jong_value = 0.0
            for gen in agent_genotypes:
                for i, g in enumerate(gen):
                    val = (g - avg_vals[i]) ** 2
                    jong_value += val
                    sum_jong += val
            jong_values.append(jong_value)
            agents_max.append(max(values))

        return max(agents_max), sum_jong


class PeaTimeStatistics(Statistics):
    @Inject("evaluation")
    def __init__(self, interval, output_file_name='fitness_pyage.txt', lowerbound=-10, upperbound=10):
        self.history = []
        self.interval = interval
        self.next_threshold = self.interval
        self.output_file_name = output_file_name
        self.output = open(self.output_file_name, 'a')
        self.lowerbound = lowerbound
        self.upperbound = upperbound
        self.start = time.time()

    def __del__(self):
        self.output.close()

    def append(self, time, best_fitness, agents_count, step_count, diversity, eval_calls, info):
        self.output.write(';'.join(map(str, [time, abs(best_fitness), step_count - 1, agents_count, diversity[0],
                                             diversity[1], eval_calls, 0.0, info])) + '\n')
        self.output.flush()

    def update(self, step_count, agents):
        try:
            best_fitness = max(a.get_fitness() for a in agents)
            logger.info(best_fitness)
            if time.time() - self.start >= self.next_threshold:
                agents_count = sum(len(a.population) for a in agents)
                eval_calls = self.evaluation._evaluate.called
                self.history.append(best_fitness)

                self.next_threshold = int(time.time() - self.start) / 100 * 100
                print self.next_threshold, time.time() - self.start
                diversity = self.compute_diversity(agents)
                best_genotype = max(agents, key=lambda a: a.get_fitness()).get_best_genotype()

                info = 'OK'
                for gene in best_genotype.genes:
                    if gene < self.lowerbound or gene > self.upperbound:
                        info = 'ERROR:' + str(gene)

                self.append(self.next_threshold, best_fitness, agents_count, step_count, diversity, eval_calls, info)
                self.next_threshold += self.interval
        except:
            logging.exception("")

    def summarize(self, agents):
        pass

    def compute_diversity(self, agents):
        agents_max = []
        sum_jong = 0.0
        jong_values = []

        for a in agents:
            agent_genotypes = [genotype.genes for genotype in a.population]
            avg_vals = [0] * len(agent_genotypes[0])
            pow_sum_vals = [0] * len(agent_genotypes[0])
            for gen in agent_genotypes:
                for i, g in enumerate(gen):
                    avg_vals[i] += g
                    pow_sum_vals[i] += g * g

            values = []
            for i in range(len(avg_vals)):
                avg_vals[i] /= len(agent_genotypes)
                values.append(sqrt(round(pow_sum_vals[i] / len(agent_genotypes) - avg_vals[i] * avg_vals[i], 7)))

            jong_value = 0.0
            for gen in agent_genotypes:
                for i, g in enumerate(gen):
                    val = (g - avg_vals[i]) ** 2
                    jong_value += val
                    sum_jong += val
            jong_values.append(jong_value)
            agents_max.append(max(values))

        return max(agents_max), sum_jong
