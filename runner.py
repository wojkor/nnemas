import argparse
import os
import time


def run(configuration, iterations):
    for _ in range(iterations):
        start = time.time()
        os.system('python -m pyage.core.bootstrap config.' + configuration)
        end = time.time()
        print 'Total time: ', str(end - start)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--conf', action="store", dest="conf", help="Config name", required=False)
    parser.add_argument('--iterations', action="store", dest="iterations", type=int, help="Iterations number", required=False)

    args = parser.parse_args()
    run(args.conf, args.iterations)