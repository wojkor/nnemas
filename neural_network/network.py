from pybrain.structure import SigmoidLayer
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer, RPropMinusTrainer
from pybrain.supervised.trainers import Trainer
from random import shuffle
from math import sqrt


class NeuralNetwork:
    def __init__(self, in_size, out_size, dataset):
        self.in_size = in_size
        self.out_size = out_size
        self.dataset = dataset

    max_fit = 0.0

    # returns input size
    @staticmethod
    def get_in_size(genes):
        return genes[0]

    # returns output size
    @staticmethod
    def get_out_size(genes):
        return genes[1]

    # returns number of hidden layers
    @staticmethod
    def get_hidden_layer_size(genes):
        return genes[2]

    # returns number of weights
    @staticmethod
    def get_weights_count(genes):
        return genes[3]

    def get_fitness(self, genes):
        network = buildNetwork(NeuralNetwork.get_in_size(genes),
                               NeuralNetwork.get_hidden_layer_size(genes),
                               NeuralNetwork.get_out_size(genes),
                               bias=False,
                               hiddenclass=SigmoidLayer)
        network._setParameters(genes[4:])

        testdata = self.dataset
        test = network.activateOnDataset(testdata)
        correct = 0
        for sample, result in zip(list(testdata), test):
            sample_class =  max(xrange(len(sample[1])), key = lambda x: sample[1][x])
            result_class = max(xrange(len(result)), key = lambda x: result[x])
            if sample_class == result_class:
                correct = correct + 1

        return float(correct)/len(test)
