import csv

from pybrain.datasets.supervised import SupervisedDataSet
from pybrain.datasets.classification import ClassificationDataSet


forest_target = {'d': 0, 'h': 1, 's': 2, 'o': 3}


def get_data_set(filename, inp, nb_classes):
    dataset = ClassificationDataSet(inp=inp, nb_classes=nb_classes, target=1)
    with open(filename, "r") as f:
        reader = csv.reader(f)
        for row in reader:
            dataset.addSample(row[1:], int(row[0]) - 1)
        dataset._convertToOneOfMany()
        return dataset
