from pybrain.datasets import ClassificationDataSet
import csv


iris_target = {"Iris-setosa": 0, "Iris-versicolor": 1, "Iris-virginica": 2}
forest_target = {'d': 0, 'h': 1, 's': 2, 'o': 3}
tic_tac_toe_target = {"positive": 0, "negative": 1}
tic_tac_toe_input = {'x': 0, 'b': 1, 'o': 2}
abalone_target = {'M': 0, 'I': 1, 'F': 2}
bankruptcy_input = {'P': 0, 'A': 1, 'N': 2}
bankruptcy_target = {"B": 0, "NB": 1}
fertility_target = {'N': 0, 'O': 1}
house_votes_input = {'n': 0, '?': 1, 'y': 2}
house_votes_target = {"democrat": 0, "republican": 1}


set_wine = ClassificationDataSet(inp=13, target=1, nb_classes=3)
with open("wine.data", "r") as f:
    reader = csv.reader(f)
    for row in reader:
        set_wine.addSample(row[1:], int(row[0]) - 1)
    set_wine._convertToOneOfMany()

set_iris = ClassificationDataSet(inp=4, target=1, nb_classes=3)
with open("iris.data", "r") as f:
    reader = csv.reader(f)
    for row in reader:
        set_iris.addSample(row[0:-1], iris_target[row[-1]])
    set_iris._convertToOneOfMany()

set_forest = ClassificationDataSet(inp=27, target=1, nb_classes=4)
with open("forest.data", "r") as f:
    reader = csv.reader(f)
    for row in reader:
        set_forest.addSample(row[1:], forest_target[row[0].replace(' ', '')])
    set_forest._convertToOneOfMany()

set_tic_tac_toe = ClassificationDataSet(inp=9, target=1, nb_classes=2)
with open("tic-tac-toe.data", "r") as f:
    reader = csv.reader(f)
    for row in reader:
        set_tic_tac_toe.addSample(
            [tic_tac_toe_input[x] for x in row[0:-1]],
            tic_tac_toe_target[row[-1]])
    set_tic_tac_toe._convertToOneOfMany()

set_abalone = ClassificationDataSet(inp=8, target=1, nb_classes=3)
with open("abalone.data", "r") as r:
    reader = csv.reader(r)
    for row in reader:
        set_abalone.addSample(row[1:], abalone_target[row[0]])
    set_abalone._convertToOneOfMany()

set_bankruptcy = ClassificationDataSet(inp=6, target=1, nb_classes=2)
with open("bankruptcy.data", "r") as r:
    reader = csv.reader(r)
    for row in reader:
        set_bankruptcy.addSample(
            [bankruptcy_input[x] for x in row[0:-1]],
            bankruptcy_target[row[-1]])
    set_bankruptcy._convertToOneOfMany()

set_fertility = ClassificationDataSet(inp=9, target=1, nb_classes=2)
with open("fertility.data", "r") as r:
    reader = csv.reader(r)
    for row in reader:
        set_fertility.addSample(row[:-1], fertility_target[row[-1]])
    set_fertility._convertToOneOfMany()

set_house_votes = ClassificationDataSet(inp=16, target=1, nb_classes=2)
with open("house-votes-84.data", "r") as r:
    reader = csv.reader(r)
    for row in reader:
        set_house_votes.addSample(
            [house_votes_input[x] for x in row[1:]],
            house_votes_target[row[0]])
    set_house_votes._convertToOneOfMany()

set_spect = ClassificationDataSet(inp=22, target=1, nb_classes=2)
with open("SPECT.data", "r") as f:
    reader = csv.reader(f)
    for row in reader:
        set_spect.addSample(row[1:], int(row[0]) - 1)
    set_spect._convertToOneOfMany()

set_spectf = ClassificationDataSet(inp=44, target=1, nb_classes=2)
with open("SPECTF.data", "r") as f:
    reader = csv.reader(f)
    for row in reader:
        set_spectf.addSample(row[1:], int(row[0]) - 1)
    set_spectf._convertToOneOfMany()

set_spambase = ClassificationDataSet(inp=57, target=1, nb_classes=2)
with open("spambase.data", "r") as f:
    reader = csv.reader(f)
    for row in reader:
        set_spambase.addSample(row[0:-1], int(row[-1]) - 1)
    set_spambase._convertToOneOfMany()

set_lung_cancer = ClassificationDataSet(inp=56, target=1, nb_classes=3)
with open("lung-cancer.data", "r") as f:
    reader = csv.reader(f)
    for row in reader:
        set_lung_cancer.addSample(row[1:], int(row[0]) - 1)
    set_lung_cancer._convertToOneOfMany()

set_arrhythmia = ClassificationDataSet(inp=279, target=1, nb_classes=16)
with open("arrhythmia.data", "r") as f:
    reader = csv.reader(f)
    for row in reader:
        set_arrhythmia.addSample(row[0:-1], int(row[-1]) - 1)
    set_arrhythmia._convertToOneOfMany()
